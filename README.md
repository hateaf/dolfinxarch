# dolfinx archlinux installation

## order of installation (excluding some python packages)

### general packages

- [x] `git` 2.31.1
- [x] `cmake` 3.20.1
- [x] `gcc` 10.2.0
- [x] `gcc-fortran` 10.2.0
- [x] `boost` 1.75.0
- [x] `boost-libs` 1.75.0
- [x] `ninja` 1.10.2

### MPI packages

- [ ] `mpich`
- [x] `openmpi` 4.0.3
- [ ] `hdf5-mpich`
- [x] `hdf5-openmpi` 1.10.4
- [x] `python-mpi4py` 3.0.3

### dolfinx easy dependecies

- [x] `openblas-lapack`
  - [x] `openblas` 0.3.13
  - [x] `lapack` 3.9.0
  - [x] `blas` 3.8.0
- [x] `xtl` 0.7.2
- [x] `xtensor` 0.23.6
- [x] `xtensor-blas` 0.19.1
- [x] `kahip`
- [x] `gmsh` 4.6.0
- [ ] `metis`
- [ ] `suitesparse` 5.8.1
- [x] `ufl`
- [x] `ffcx`
- [x] `basix`
- [x] `adios2` 2.7.1

### dolfinx difficult dependencies

- [ ] `parmetis`
- [x] `petsc` 3.15
  - [x] `download-suitesparse`
  - [x] `download-hypre`
  - [x] `download-metis`
  - [x] `download-mumps`
  - [x] `download-ptscotch`
  - [x] `download-scalapack`
  - [x] `download-spai`
  - [x] `download-superlu`
  - [x] `download-superlu_dist`
  - [x] `with-scalar-type=real`
- [ ] `petsc4py`
- [x] `slepc` 3.15

### dolfinx

- [x] `dolfinx`
